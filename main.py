from hand_pose_vis.projection import *
from hand_pose_vis.csv_reader import *
from hand_pose_vis.visualization_2d_3d import *

if __name__ == "__main__":

    generate_random_cameras = False
    data_step_size = 100

    path_to_data = "ExampleCapture.csv"

    # load data
    data = load_optitrack_csv(path_to_data, normalize_data=True)

    # reorder joints if necessary
    T = list(range(16, 20))
    I = list(range(0, 4))
    M = list(range(8, 12))
    R = list(range(12, 16))
    L = list(range(4, 8))
    W = [20]
    data = reorder_joints(data, W+T+I+M+R+L)

    if generate_random_cameras:
        # generate some camera matrices
        cameras = []
        for i in range(3):

            # sample random camera position
            phi = np.random.rand(1) * 2*np.pi
            theta = np.random.rand(1) * np.pi
            r = np.random.rand(1) * 10 + 1 # add one to ensure that the camera is ouside of the unit ball (hand is inside because of normalization)
            center = r * [np.sin(theta) * np.cos(theta), np.sin(theta) * np.sin(phi), np.cos(theta)]

            # intialize camera with default parameters (except center)
            camera = Pinhole_camera_matirx(camera_position=center)

            # sample random up direction
            phi = np.random.rand(1) * 2*np.pi
            theta = np.random.rand(1) * np.pi
            up = np.squeeze(np.array([np.sin(theta) * np.cos(theta), np.sin(theta) * np.sin(phi), np.cos(theta)]))

            # make camera look at centroid
            camera.look_at(x=[0, 0, 0], up=up)

            # add camera to list
            cameras += [camera]
    else:
        # generate cameras looking form each axis to the center

        camera_x = Pinhole_camera_matirx(camera_position=[1, 0, 0])
        camera_x.look_at(x=[0, 0, 0], up=[0, 0, 1])
        camera_y = Pinhole_camera_matirx(camera_position=[0, 1, 0])
        camera_y.look_at(x=[0, 0, 0], up=[0, 0, 1])
        camera_z = Pinhole_camera_matirx(camera_position=[0, 0, 1])
        camera_z.look_at(x=[0, 0, 0], up=[1, 0, 0])

        cameras = [camera_x, camera_y, camera_z]

    num_cameras = len(cameras)


    # define data generator
    def generator():

        while True:
            for X in data[0:-1:data_step_size]:
                for camera in cameras:
                    x = camera(X)
                    yield(x, X)

    def init_figure(f=None):

        if not f:
            f = plt.figure()

        ax_3d = f.add_subplot(1, num_cameras + 1, 1, projection='3d')
        ax_3d.clear()
        ax_3d.set_xlabel('x')
        ax_3d.set_ylabel('y')
        ax_3d.set_zlabel('z')
        ax_3d.set_xlim3d(-1, 1)
        ax_3d.set_ylim3d(-1, 1)
        ax_3d.set_zlim3d(-1, 1)

        ax_cameras = []
        for i in range(num_cameras):
            ax = f.add_subplot(1, num_cameras + 1, i + 2)
            ax.clear()
            ax.set_xlim(-1, 1)
            ax.set_ylim(-1, 1)
            ax.set_aspect('equal')
            ax.set_xlabel('x')
            ax.set_ylabel('y')
            ax.invert_yaxis()
            ax_cameras += [ax]

        return f, ax_3d, ax_cameras

    g = generator()
    f = None
    while True:

        f, ax_3d, ax_cameras = init_figure(f)

        for j in range(num_cameras):
            (x2d, x3d) = next(g)

            # only need to draw 3d hand once!
            if j == 0:
                plot_3d(x3d.T, fig=f, axis=ax_3d)

            plot_sample(sample={'keypoints': x2d.T}, ax=ax_cameras[j])
        f.canvas.draw()

        # only continue if keyboard button was pressed (not mouse)
        while not plt.waitforbuttonpress(-1):
            pass






