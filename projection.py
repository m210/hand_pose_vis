import numpy as np


def to_array(X):
    X = np.array(X)
    if len(X.shape) == 1:
        X = np.expand_dims(X, 1)
    if len(X.shape) != 2:
        raise Exception("X has unexpected shape.")

    return X


def to_homogeneous(X, dim):
    # make X a 2d array
    X = to_array(X)

    # make X homogeneous
    if X.shape[0] == dim:
        X = np.vstack((X, np.ones((1,X.shape[1]))))
    if X.shape[0] != dim+1:
        raise Exception("X has unexpected number of rows.")

    return X


def normalize(X, dim):
    # make X a 2d array
    X = to_array(X)
    if X.shape[0] != dim+1:
        raise Exception("X has unexpected number of rows.")
    return X / X[-1,:]


def from_homogeneous(X, dim):
    X = normalize(X, dim)
    return X[0:-1, :]


class Projection:

    def __init__(self, P=np.eye(3, 4)):
        self.P = P

    def __call__(self, X, normalize_output=True, short_output=True):
        X = to_homogeneous(X, 3)
        x = np.matmul(self.P, X)

        if short_output:
            return from_homogeneous(x, 2)

        if normalize_output:
            return normalize(x, 2)

        return x


class Pinhole_camera_matirx(Projection):
    def __init__(self, pixel_dim=[1, 1], focal_length=1, skew=0, image_center=[0, 0], camera_position=[0, 0, 0], camera_frame=np.eye(3)):
        super().__init__()
        self.pixel_dim = pixel_dim
        self.focal_length = focal_length
        self.skew = skew
        self.image_center = image_center
        self.camera_position = camera_position
        self.camera_frame = camera_frame
        self.compute_projection_matrix()

    def compute_projection_matrix(self):
        # compute intrinsic parameters
        self.K = np.zeros((3, 3))
        self.K[0, 0] = self.focal_length * self.pixel_dim[0]
        self.K[1, 1] = self.focal_length * self.pixel_dim[1]
        self.K[0, 1] = self.skew
        self.K[0:2, 2] = self.image_center
        self.K[2, 2] = 1

        # compute extrinsic parameters
        self.RT = np.zeros((3, 4))
        R = np.transpose(self.camera_frame)
        self.RT[:, 0:3] = R
        self.RT[:, 3] = -np.matmul(R, self.camera_position).ravel()

        # assemble camera matrix
        self.P = np.matmul(self.K, self.RT)

    def look_at(self, x, up=[]):
        x = np.array(x)
        down = self.camera_frame[:, 1] if up==[] else -np.array(up)
        forward = x - np.squeeze(self.camera_position)
        forward = forward / np.linalg.norm(forward)
        right = np.cross(down, forward)
        down = np.cross(forward, right)
        self.camera_frame[:, 0] = right
        self.camera_frame[:, 1] = down
        self.camera_frame[:, 2] = forward

        self.compute_projection_matrix()


if __name__ == "__main__":
    camera = Pinhole_camera_matirx(pixel_dim=[1, 1], focal_length=1, skew=0, image_center=[0, 0], camera_position=[0, 0, 0], camera_frame=np.eye(3))

    print(camera.P, "\n")

    print(camera(np.transpose([[1, 0, 1]])), "\n")

    camera.look_at([1, 0, 1])
    print(camera.P, "\n")

    print(camera(np.transpose([[1, 0, 1]])), "\n")

