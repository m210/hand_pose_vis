from matplotlib.ticker import NullLocator
import matplotlib.patches as patches
from matplotlib import colors as mcolors
from matplotlib import cm
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

"""
hard coded list of edges for hand. Joints should be ordered as follows:
W, T0-3, I0-3, M0-3, R0-3, L0-3 
"""
edges = [[0, 1], [1, 2], [2, 3], [3, 4], [0, 5], [5, 6], [6, 7], [7, 8],
             [0, 9], [9, 10], [10, 11], [11, 12], [0, 13], [13, 14], [14, 15],
             [15, 16], [0, 17], [17, 18], [18, 19], [19, 20]]


def plot_3d(xyz, linewidth='1', fig=None, axis=None):

    if not fig:
        fig = plt.figure()
    if not axis:
        axis = fig.add_subplot(111, projection='3d')

    colors = np.array([[0., 0., 0.5],
                     [0., 0., 0.73172906],
                     [0., 0., 0.96345811],
                     [0., 0.12745098, 1.],
                     [0., 0.33137255, 1.],
                     [0., 0.55098039, 1.],
                     [0., 0.75490196, 1.],
                     [0.06008855, 0.9745098, 0.90765338],
                     [0.22454143, 1., 0.74320051],
                     [0.40164453, 1., 0.56609741],
                     [0.56609741, 1., 0.40164453],
                     [0.74320051, 1., 0.22454143],
                     [0.90765338, 1., 0.06008855],
                     [1., 0.82861293, 0.],
                     [1., 0.63979666, 0.],
                     [1., 0.43645606, 0.],
                     [1., 0.2476398, 0.],
                     [0.96345811, 0.0442992, 0.],
                     [0.73172906, 0., 0.],
                     [0.5, 0., 0.]])

    # define connections and colors of the bones
    bones = [((0, 1), colors[0, :]),
            ((1, 2), colors[1, :]),
            ((2, 3), colors[2, :]),
            ((3, 4), colors[3, :]),

            ((0, 5), colors[4, :]),
            ((5, 6), colors[5, :]),
            ((6, 7), colors[6, :]),
            ((7, 8), colors[7, :]),

            ((0, 9), colors[8, :]),
            ((9, 10), colors[9, :]),
            ((10, 11), colors[10, :]),
            ((11, 12), colors[11, :]),

            ((0, 13), colors[12, :]),
            ((13, 14), colors[13, :]),
            ((14, 15), colors[14, :]),
            ((15, 16), colors[15, :]),

            ((0, 17), colors[16, :]),
            ((17, 18), colors[17, :]),
            ((18, 19), colors[18, :]),
            ((19, 20), colors[19, :])]

    for ie, (connection, color) in enumerate(bones):
        coord1 = xyz[connection[0], :]
        coord2 = xyz[connection[1], :]
        coords = np.stack([coord1, coord2])
        rgb = mcolors.hsv_to_rgb([ie / float(len(edges)), 1.0, 1.0])
        axis.plot(coords[:, 0], coords[:, 1], coords[:, 2], color=rgb, linewidth=linewidth)

    axis.view_init(azim=-90., elev=90.)
    return fig


def plot_sample(sample, ax=None):

    img = sample.get('image', None)
    kp = sample['keypoints']

    if not ax:
        if 'normalized_2d' in sample:
            fig = plt.figure(figsize=(8, 4))
            ax = fig.add_subplot(1, 2, 1)
            ax_norm = fig.add_subplot(1, 2, 2)
        else:
            fig = plt.figure(figsize=(4, 4))
            ax = fig.add_subplot(1, 1, 1)

        ax.axis('off')
    else:
        fig = None

    if img:
        ax.imshow(img)

    # plot keypoints as dots and label them
    for p in range(kp.shape[0]):
        rgb = mcolors.hsv_to_rgb([p / float(kp.shape[0]), 1.0, 1.0])
        if kp[p, 0] > 0 and kp[p, 1] > 0:
            ax.scatter(kp[p, 0], kp[p, 1], s=10, marker='*', c=[rgb])
        if 'normalized_2d' in sample:
            ax_norm.scatter(sample['normalized_2d'][p, 0],
                            sample['normalized_2d'][p, 1], marker='*', c=[rgb])

    # draw edges between keypoints
    for ie, e in enumerate(edges):
        rgb = mcolors.hsv_to_rgb([ie/float(len(edges)), 1.0, 1.0])
        ax.plot(kp[e, 0], kp[e, 1], color=rgb)
        if 'normalized_2d' in sample:
            ax_norm.plot(sample['normalized_2d'][e, 0],
                         sample['normalized_2d'][e, 1], color=rgb)

    if 'yoloTgt' in sample:
        h, w = img.shape[0], img.shape[1]
        bw = sample['yoloTgt'][0, 3]*w
        bh = sample['yoloTgt'][0, 4]*h
        x1 = (sample['yoloTgt'][0, 1]*w)-(bw/2)
        y1 = (sample['yoloTgt'][0, 2]*h)-(bh/2)
        bbox = patches.Rectangle(
            (x1, y1), bw, bh, linewidth=2, edgecolor='g', facecolor='none')
        ax.add_patch(bbox)

    plt.tight_layout()
    return fig



